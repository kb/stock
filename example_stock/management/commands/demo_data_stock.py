# -*- encoding: utf-8 -*-
from decimal import Decimal
from django.core.management.base import BaseCommand

from stock.models import Product, ProductCategory, ProductType


class Command(BaseCommand):

    help = "Create demo data for the 'stock' app"

    def _init_product(self, slug, name, price, category):
        return Product.objects.init_product(slug, name, "", price, category)

    def _init_product_category(self, slug, name, product_type):
        return ProductCategory.objects.init_product_category(
            slug, name, product_type
        )

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        # product type
        ambient = ProductType.objects.init_product_type("ambient", "Ambient")
        frozen = ProductType.objects.init_product_type("frozen", "Frozen")
        # product category
        ready_meals = self._init_product_category(
            "ready", "Ready Meals", frozen
        )
        ice_cream = self._init_product_category(
            "ice-cream", "Ice Cream", frozen
        )
        herbs = self._init_product_category("herbs", "Herbs", ambient)
        soup = self._init_product_category("soup", "Soup", ambient)
        # products
        self._init_product(
            "cottage-pie", "Cottage Pie", Decimal("4.50"), ready_meals
        )
        self._init_product("lasagne", "Lasagne", Decimal("3.50"), ready_meals)
        self._init_product("fish-pie", "Fish Pie", Decimal("5.50"), ready_meals)
        self._init_product(
            "raspberry-ripple", "Raspberry Ripple", Decimal("3.75"), ice_cream
        )
        self._init_product(
            "mint-choc-chip", "Mint Choc Chip", Decimal("3.55"), ice_cream
        )
        self._init_product(
            "rum-raisin", "Rum and Raisin", Decimal("3.35"), ice_cream
        )
        self._init_product("G101", "Garlic Powder", Decimal("5.50"), herbs)
        self._init_product("B100", "Basil", Decimal("5.00"), herbs)
        self._init_product("T111", "Thyme", Decimal("5.00"), herbs)
        self._init_product("P312", "Parsely", Decimal("4.00"), herbs)
        self._init_product("soup-tomato", "Tomato Soup", Decimal("2.50"), soup)
        self._init_product("soup-onion", "Onion Soup", Decimal("2.30"), soup)
        self.stdout.write("{} - Complete".format(self.help))
