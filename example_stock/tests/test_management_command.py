# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_stock_export_as_csv():
    call_command("demo_data_stock")
    call_command("stock_export_as_csv")


@pytest.mark.django_db
def test_demo_data_stock():
    call_command("demo_data_stock")
