# -*- encoding: utf-8 -*-
import csv

from django.core.management.base import BaseCommand

from stock.models import Product


class Command(BaseCommand):

    help = "Export 'stock' as a CSV file"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        file_name = "stock_export_as_csv.csv"
        with open(file_name, "w", newline="") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            for x in Product.objects.all().order_by("pk"):
                csv_writer.writerow(
                    [
                        x.pk,
                        x.slug,
                        x.name,
                        x.price,
                        x.category.pk,
                        x.category.slug,
                        x.category.name,
                        x.category.product_type.pk,
                        x.category.product_type.slug,
                        x.category.product_type.name,
                    ]
                )
            self.stdout.write(
                "{}: '{}' - Complete".format(self.help, file_name)
            )
