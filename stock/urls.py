# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import ProductCreateView, ProductListView, ProductUpdateView


urlpatterns = [
    re_path(
        r"^product/create/$",
        view=ProductCreateView.as_view(),
        name="stock.product.create",
    ),
    re_path(
        r"^product/$",
        view=ProductListView.as_view(),
        name="stock.product.list",
    ),
    re_path(
        r"^product/(?P<pk>\d+)/update/$",
        view=ProductUpdateView.as_view(),
        name="stock.product.update",
    ),
]
