# -*- encoding: utf-8 -*-
from django.utils.text import slugify

from base.tests.model_maker import clean_and_save
from stock.models import ProductCategory, ProductType


def make_product_category(name, slug, product_type, **kwargs):
    defaults = dict(name=name, slug=slugify(slug), product_type=product_type)
    defaults.update(kwargs)
    return clean_and_save(ProductCategory(**defaults))


def make_product_type(name, slug, **kwargs):
    defaults = dict(name=name, slug=slugify(slug))
    defaults.update(kwargs)
    return clean_and_save(ProductType(**defaults))
