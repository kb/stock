# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_init_app_stock():
    call_command("init_app_stock")
