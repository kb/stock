# -*- encoding: utf-8 -*-
import pytest

from stock.models import ProductCategory
from .factories import (
    ProductCategoryFactory,
    ProductFactory,
    ProductTypeFactory,
)


@pytest.mark.django_db
def test_str():
    str(ProductCategoryFactory())


@pytest.mark.django_db
def test_product_type():
    product_type = ProductTypeFactory()
    ProductCategoryFactory()
    ProductCategoryFactory(product_type=product_type)
    ProductCategoryFactory(product_type=product_type)
    categories = ProductCategory.objects.product_type(product_type.slug)
    assert 2 == categories.count()


@pytest.mark.django_db
def test_products():
    category = ProductCategoryFactory()
    ProductFactory(category=category, name="a")
    ProductFactory(category=ProductCategoryFactory(), name="b")
    ProductFactory(category=category, name="c")
    assert set(["a", "c"]) == {x.name for x in category.products()}
